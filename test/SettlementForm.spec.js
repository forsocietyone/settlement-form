import { mount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import sinon from 'sinon';
import SettlementForm from '@/components/SettlementForm.vue'
import { state as storeState, mutations as storeMutations } from '@/store/index.js'
import VueRouter from 'vue-router'

const localVue = createLocalVue()
localVue.use(Vuex)

const router = new VueRouter()
localVue.use(VueRouter)

const store = new Vuex.Store({
  state: storeState,
  mutations: storeMutations
})

describe('SettlementForm', () => {
  it('is a Vue instance', () => {
    const wrapper = mount(SettlementForm)

    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('handles form submission', () => {
    const wrapper = mount(SettlementForm),
      methodStub = sinon.stub()

    wrapper.setMethods({ handleFormSubmit: methodStub })
    wrapper.find('form').trigger('submit.capture.prevent')

    expect(methodStub.called).toBe(true)
  });

  it('stores submitted details', () => {
    const wrapper = mount(SettlementForm, { localVue, store, router }),
      expected = 'Test name'

    wrapper.setData({ form: { name: expected } })
    wrapper.find('form').trigger('submit.capture.prevent')

    expect(wrapper.vm.$store.state.form.name).toBe(expected)
  });

  it('navigates to submitted page on submit', () => {
    const wrapper = mount(SettlementForm, { localVue, store, router })

    wrapper.setData({ form: { name: 'Name' } })
    wrapper.vm.handleFormSubmit()

    expect(wrapper.vm.$router.history.current.path).toBe('/submitted')
  });
})
