# Settlement Form

## Dependencies

* docker (tested on version 1.12.1, build 23cf638)
* docker-compose (tested on version 1.8.0, build f3628c7)

## Building/Running

### Initial Setup

```
make init
```

This will clean up the docker environment, pull and build the required images, and tell npm to install packages.

### Running a live server

```
make run
```

This will use node (via nuxt) to serve a live hot-reload environment for the project.

Once running, you can access it via ``localhost:9000``.

### Stopping the live server

```
make stop
```

This will have docker-compose stop and kill any running services.