export const state = () => ({
  form: {}
})

export const mutations = {
  submit(state, details) {
    state.form = details

    localStorage.setItem('form', JSON.stringify(details));
  },
  clear(state) {
    state.form = {}

    localStorage.removeItem('form');
  },
}