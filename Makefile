COMPOSE_FILE?=docker-compose.yml
COMPOSE_PROJECT_NAME=settlement-form
COMPOSE=docker-compose -p $(COMPOSE_PROJECT_NAME) -f $(COMPOSE_FILE)

init: clean build app-build

clean:
	$(COMPOSE) kill
	$(COMPOSE) rm --force

build:
	$(COMPOSE) pull
	$(COMPOSE) build

app-build:
	$(COMPOSE) run --rm node npm install

# example:
# C=install package make npm
npm: C?=
npm:
	$(COMPOSE) run --rm node npm $(C)

# example:
# C=install package make npx
npx: C?=
npx:
	$(COMPOSE) run --rm node npx $(C)

run:
	$(COMPOSE) up -d app

stop:
	$(COMPOSE) kill

tests:
	$(COMPOSE) run --rm node npm run test

.PHONY: tests